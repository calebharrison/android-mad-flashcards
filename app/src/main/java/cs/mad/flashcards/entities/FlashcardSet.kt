package cs.mad.flashcards.entities

import androidx.room.*

@Entity
data class FlashcardSet(
    @PrimaryKey(autoGenerate = true) val myId: Long?,
    val title: String
)

interface FlashcardSetDao {
    @Query("select * from FlashcardSet order by lower(title) asc")
    fun getAll(): List<FlashcardSet>

    @Insert
    fun insert(vararg flashcardSet: FlashcardSet)

    @Insert
    fun insert(flashcardSets: List<FlashcardSet>)

    @Update
    fun update(flashcardSet: FlashcardSet)

    @Delete
    fun delete(flashcardSet: FlashcardSet)
}