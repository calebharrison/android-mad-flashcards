package cs.mad.flashcards.entities

import androidx.room.*

@Entity
data class Flashcard(
        @PrimaryKey(autoGenerate = true) val myId: Long?,
        //@SerializedName("id") val outsideId: Long?,
        val question: String,
        val answer: String
)

@Dao
interface FlashcardDao {
    @Query("select * from Flashcard order by lower(question) asc")
    fun getAll(): List<Flashcard>

    @Insert
    fun insert(vararg flashcard: Flashcard)

    @Insert
    fun insert(flashcards: List<Flashcard>)

    @Update
    fun update(flashcard: Flashcard)

    @Delete
    fun delete(flashcard: Flashcard)
}