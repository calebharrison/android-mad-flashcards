package cs.mad.flashcards.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import cs.mad.flashcards.adapters.FlashcardSetAdapter
import cs.mad.flashcards.data.FlashcardDatabase
import cs.mad.flashcards.databinding.ActivityMainBinding
import androidx.lifecycle.lifecycleScope
import kotlinx.coroutines.*
import cs.mad.flashcards.entities.FlashcardSet
//import cs.mad.flashcards.entities.getHardcodedFlashcardSets

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private val setDao by lazy {FlashcardDatabase.getDatabase(applicationContext).setDao()}
    private val cardDao by lazy {FlashcardDatabase.getDatabase(applicationContext).cardDao()}

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        loadFromDb()

//        binding.createSetButton.setOnClickListener {
//            (binding.flashcardSetList.adapter as FlashcardSetAdapter).addItem(FlashcardSet("test"))
//            binding.flashcardSetList.smoothScrollToPosition((binding.flashcardSetList.adapter as FlashcardSetAdapter).itemCount - 1)
//        }
    }

    private fun loadFromDb() {
        lifecycleScope.launch {
            (binding.flashcardSetList.adapter as FlashcardSetAdapter).setData(setDao.getAll())
        }
    }
}