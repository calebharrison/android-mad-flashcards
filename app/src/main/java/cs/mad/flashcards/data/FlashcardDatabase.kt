package cs.mad.flashcards.data

import android.content.Context
import androidx.room.*
import cs.mad.flashcards.entities.*

@Database(entities = [Flashcard::class, FlashcardSet::class], version = 1)
abstract class FlashcardDatabase: RoomDatabase() {
    abstract fun setDao(): FlashcardSetDao
    abstract fun cardDao(): FlashcardDao

    companion object {
        // Singleton prevents multiple instances of database opening at the
        // same time.
        @Volatile
        private var INSTANCE: FlashcardDatabase? = null

        fun getDatabase(context: Context): FlashcardDatabase {
            // if the INSTANCE is not null, then return it,
            // if it is, then create the database
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    FlashcardDatabase::class.java,
                    "app_database"
                ).fallbackToDestructiveMigration().build()
                INSTANCE = instance
                // return instance
                instance
            }
        }
    }
}